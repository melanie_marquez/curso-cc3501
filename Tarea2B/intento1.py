import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys
import transformations as tr
import basic_shapes as bs
import easy_shaders as es
import cursos_malla_dcc as cmd

class Controller:
    def __init__(self):
        self.fillPolygon = True
        self.pasosX = 0
        self.pasosY = 10
        self.destacadoX = 0
        self.destacadoY = 0
        self.destacados = []
        self.destacados2 = [1, 0, 1, 1, 1, 2]
        self.irA = False
        self.curva = []
        self.seguimiento = False
 
controller = Controller()

def on_key(window_obj, key, scancode, action, mods):
    global controller

    if action == glfw.REPEAT or action == glfw.PRESS:
        ramos = cmd.cursos()
        # Move the camera position
        if key == glfw.KEY_LEFT:
            if controller.seguimiento == False:
                controller.pasosX -= 0.1

        elif key == glfw.KEY_RIGHT:
            if controller.seguimiento == False:
                controller.pasosX += 0.1

        elif key == glfw.KEY_UP:
            if controller.seguimiento == False:
                controller.pasosY -= 0.1
    
        elif key == glfw.KEY_DOWN:
            if controller.seguimiento == False:
                controller.pasosY += 0.1

        elif key == glfw.KEY_D:
            controller.destacados = []
            controller.destacados2 = []
            if controller.destacadoX == 5:
                controller.destacadoX = 5
            elif controller.destacadoY == 10 and controller.destacadoX == 2:
                controller.destacadoX = 2
            elif (controller.destacadoY == 5 or controller.destacadoY == 7 or controller.destacadoY == 9) and controller.destacadoX == 4:
                controller.destacadoX = 4
            else:
                controller.destacadoX += 1
            if controller.seguimiento == True:
                controller.irA = True
            imprimirDatos()
                

        elif key == glfw.KEY_A:
            controller.destacados = []
            controller.destacados2 = []
            if controller.destacadoX == 0:
                controller.destacadoX = 0
            elif (controller.destacadoY == 5 or controller.destacadoY == 7 or controller.destacadoY == 9) and controller.destacadoX == 0:
                controller.destacadoX = 0
            else:
                controller.destacadoX -= 1
            if controller.seguimiento == True:
                controller.irA = True
            imprimirDatos()

        elif key == glfw.KEY_W:
            controller.destacados = []
            controller.destacados2 = []
            if controller.destacadoY == 0:
                controller.destacadoY = 0
            elif (controller.destacadoY == 6 or controller.destacadoY == 8) and controller.destacadoX == 5:
                controller.destacadoY = controller.destacadoY
            else:
                controller.destacadoY -= 1
            if controller.seguimiento == True:
                controller.irA = True
            imprimirDatos()
        elif key == glfw.KEY_S:
            controller.destacados = []
            controller.destacados2 = []
            if controller.destacadoY == 10:
                controller.destacadoY = 10
            elif controller.destacadoY == 9 and (controller.destacadoX == 4 or controller.destacadoX == 3):
                controller.destacadoY = controller.destacadoY
            elif (controller.destacadoY == 4 or controller.destacadoY == 6 or controller.destacadoY == 8 ) and controller.destacadoX == 5:
                controller.destacadoY = controller.destacadoY
            else:
                controller.destacadoY += 1
            if controller.seguimiento == True:
                controller.irA = True
            imprimirDatos()

        elif key == glfw.KEY_1:
            controller.irA = True
        elif key == glfw.KEY_2:
            controller.seguimiento = not controller.seguimiento


        R = ramos[controller.destacadoY][controller.destacadoX]
        req = R.split("-")
        requisitos = req[3]
        if requisitos != "None":
            requisitos = requisitos.split(",")
            for k in range(len(requisitos)):
                for i in range(11):
                    for j in range(len(ramos[i])):
                        ramo = ramos[i][j].split("-")
                        if requisitos[k] == ramo[0]:
                            controller.destacados.append(i)
                            controller.destacados.append(j)

        requisitoDe = req[4]
        if requisitoDe != "None":
            requisitoDe = requisitoDe.split(",")
            for k in range(len(requisitoDe)):
                for i in range(11):
                    for j in range(len(ramos[i])):
                        ramo = ramos[i][j].split("-")
                        if requisitoDe[k] == ramo[0]:
                            controller.destacados2.append(i)
                            controller.destacados2.append(j)

    if action != glfw.PRESS:
        return

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_ESCAPE:
        sys.exit()

def crearCubo(image_filename):

    # Defining locations and texture coordinates for each vertex of the shape  
    vertices = [
    #   positions         texture coordinates
    # Z+: number 1
        -0.5, -0.5,  0.5, 0, 1/3,
         0.5, -0.5,  0.5, 1/2, 1/3,
         0.5,  0.5,  0.5, 1/2, 0,
        -0.5,  0.5,  0.5, 0, 0,

    # Z-: number 6
        -0.5, -0.5, -0.5, 1/2, 1,
         0.5, -0.5, -0.5, 1, 1,
         0.5,  0.5, -0.5, 1, 2/3,
        -0.5,  0.5, -0.5, 1/2, 2/3,
        
    # X+: number 5
         0.5, -0.5, -0.5, 0, 1,
         0.5,  0.5, -0.5, 1/2, 1,
         0.5,  0.5,  0.5, 1/2, 2/3,
         0.5, -0.5,  0.5, 0, 2/3,
 
    # X-: number 2
        -0.5, -0.5, -0.5, 1/2, 1/3,
        -0.5,  0.5, -0.5, 1, 1/3,
        -0.5,  0.5,  0.5, 1, 0,
        -0.5, -0.5,  0.5, 1/2, 0,

    # Y+: number 4
        -0.5,  0.5, -0.5, 1/2, 2/3,
         0.5,  0.5, -0.5, 1, 2/3,
         0.5,  0.5,  0.5, 1, 1/3,
        -0.5,  0.5,  0.5, 1/2, 1/3,

    # Y-: number 3
        -0.5, -0.5, -0.5, 0, 2/3,
         0.5, -0.5, -0.5, 1/2, 2/3,
         0.5, -0.5,  0.5, 1/2, 1/3,
        -0.5, -0.5,  0.5, 0, 1/3
        ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
          0, 1, 2, 2, 3, 0, # Z+
          7, 6, 5, 5, 4, 7, # Z-
          8, 9,10,10,11, 8, # X+
         15,14,13,13,12,15, # X-
         19,18,17,17,16,19, # Y+
         20,21,22,22,23,20] # Y-

    return bs.Shape(vertices, indices, image_filename)

def enDestacados(x,y):
    if len(controller.destacados) != 0:
        for i in range(len(controller.destacados)):
            if i%2 == 0:
                des1 = controller.destacados[i]
                des2 = controller.destacados[i+1]
                if x == des1 and y == des2:
                    return True
    return False

def enDestacados2(x,y):
    if len(controller.destacados2) != 0:
        for i in range(len(controller.destacados2)):
            if i%2 == 0:
                des1 = controller.destacados2[i]
                des2 = controller.destacados2[i+1]
                if x == des1 and y == des2:
                    return True
    return False

def imprimirDatos():
    ramos = cmd.cursos()
    R = ramos[controller.destacadoY][controller.destacadoX]
    R = R.split("-")
    codigo = R[0]
    nombre = R[1]
    creditos = R[2]
    requisitos = R[3].split(",")
    requisitoDe = R[4].split(",")
    print("Nombre: " + nombre)
    print("Codigo: " + codigo)
    print("Creditos: " + creditos)
    print("Requisitos: ")
    for i in requisitos:
        if i != "None":
            print(i)
        else:
            print("Ninguno")
    print("Ramos que tienen como requisito este ramo:")
    for i in requisitoDe:
        if i != "None":
            print(i)
        else:
            print("Ninguno")

if __name__ == "__main__":
    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 800
    height = 800

    window = glfw.create_window(width, height, "Malla curricular uwu", None, None)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Creating shader programs for textures and for colores
    textureShaderProgram = es.SimpleTextureModelViewProjectionShaderProgram()
    colorShaderProgram = es.SimpleModelViewProjectionShaderProgram()

    # Setting up the clear screen color
    glClearColor(0.85, 0.85, 0.85, 1.0)

    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)

    # Creating shapes on GPU memory

    gpuCubo = []
    for i in range(12):
        gpuCubo.append([])
    for j in range(7):
        gpuCubo[i].append(None)

    cursos = cmd.cursos()
    for i in range(11):
        for j in range(len(cursos[i])):
            R = cursos[i][j].split("-")
            nombreArchivo = R[0] + "_1.jpg"
            gpuCubo[i].append(es.toGPUShape(crearCubo(nombreArchivo),GL_REPEAT, GL_LINEAR))
            

    gpuCuboDestacado = []
    for i in range(12):
        gpuCuboDestacado.append([])
    for j in range(7):
        gpuCuboDestacado[i].append(None)

    for i in range(11):
        for j in range(len(cursos[i])):
            R = cursos[i][j].split("-")
            nombreArchivo = R[0] + "_2.jpg"
            gpuCuboDestacado[i].append(es.toGPUShape(crearCubo(nombreArchivo),GL_REPEAT, GL_LINEAR))

    gpuCuboRequisitos = []
    for i in range(12):
        gpuCuboRequisitos.append([])
    for j in range(7):
        gpuCuboRequisitos[i].append(None)

    for i in range(11):
        for j in range(len(cursos[i])):
            R = cursos[i][j].split("-")
            nombreArchivo = R[0] + "_3.jpg"
            gpuCuboRequisitos[i].append(es.toGPUShape(crearCubo(nombreArchivo),GL_REPEAT, GL_LINEAR))

    gpuCuboRequisitosDe = []
    for i in range(12):
        gpuCuboRequisitosDe.append([])
    for j in range(7):
        gpuCuboRequisitosDe[i].append(None)

    for i in range(11):
        for j in range(len(cursos[i])):
            R = cursos[i][j].split("-")
            nombreArchivo = R[0] + "_4.jpg"
            gpuCuboRequisitosDe[i].append(es.toGPUShape(crearCubo(nombreArchivo),GL_REPEAT, GL_LINEAR))

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        #Camara en un plano superior
        view = tr.lookAt(
            np.array([-1*controller.pasosX,controller.pasosY,5]),
            np.array([-1*controller.pasosX,controller.pasosY-10,0]),
            np.array([0,0,1])
        )
        if controller.irA == True:
            x = controller.destacadoX
            y = controller.destacadoY
            controller.pasosX = controller.destacadoX*1.3
            controller.pasosY = controller.destacadoY*1.4 + 7
            controller.irA = False
        
        if controller.seguimiento == True:
                controller.irA = True

        # Create projection
        projection = tr.perspective(45, float(width) / float(height), 0.1, 100)

        # Drawing dice (with texture, another shader program)
        glUseProgram(textureShaderProgram.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "view"), 1, GL_TRUE, view)

        ramos = cmd.cursos()
        for i in range(11):
            for j in range(len(ramos[i])):
                if j == controller.destacadoX and i == controller.destacadoY:
                    model = tr.translate(j*-1.3 ,i*1.3,1)
                    glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "model"), 1, GL_TRUE, model)
                    textureShaderProgram.drawShape(gpuCuboDestacado[i][j])
                elif enDestacados(i,j):
                    model = tr.translate(j*-1.3,i*1.3,1)
                    glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "model"), 1, GL_TRUE, model)
                    textureShaderProgram.drawShape(gpuCuboRequisitos[i][j])
                elif enDestacados2(i,j):
                    model = tr.translate(j*-1.3,i*1.3,1)
                    glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "model"), 1, GL_TRUE, model)
                    textureShaderProgram.drawShape(gpuCuboRequisitosDe[i][j])
                else:
                    model = tr.translate(j*-1.3,i*1.3,0)
                    glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "model"), 1, GL_TRUE, model)
                    textureShaderProgram.drawShape(gpuCubo[i][j])

        # Once the drawing is rendered, buffers are swap so an uncomplete drawing is never seen.
        glfw.swap_buffers(window)

    glfw.terminate()
