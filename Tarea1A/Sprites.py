#Pokeball
def pokeball():
    Pokeball = []
    for i in range(20):
        Pokeball.append([])
        for j in range(20):
            Pokeball[i].append(0)

    for i in range(20):
        for j in range(20):
            if j == 3:
                if (i>5 and i<10):
                    Pokeball[i][j] = 1
            if j == 4:
                if i == 4 or i == 5 or i == 10 or i == 11:
                    Pokeball[i][j] = 1
                if i>5 and i<10:
                    Pokeball[i][j] = 2
            if j == 5:
                if i == 3 or i == 12:
                    Pokeball[i][j] = 1
                if i>3 and i<12:
                    Pokeball[i][j] = 2
            if j == 6:
                if i == 2 or i == 13:
                    Pokeball[i][j] = 1
                if (i>2 and i<13):
                    Pokeball[i][j] = 2
            if j == 7:
                if i == 2 or i == 13:
                    Pokeball[i][j] = 1
                if (i>2 and i<13):
                    Pokeball[i][j] = 2
            if j == 8:
                if i == 1 or i == 14 or i == 7 or i == 8:
                    Pokeball[i][j] = 1
                if (i>1 and i<7) or (i>8 and i<14):
                    Pokeball[i][j] = 2
            if j == 9:
                if i == 1 or i == 14 or i == 6 or i == 9:
                    Pokeball[i][j] = 1
                if (i>1 and i<6) or (i>9 and i<14):
                    Pokeball[i][j] = 2
                elif i>6 and i<9:
                    Pokeball[i][j] = 3
            if j == 10:
                if (i >= 1 and i<=6) or (i >= 9 and i <= 14):
                    Pokeball[i][j] = 1
                if i>6 and i<9:
                    Pokeball[i][j] = 3
            if j == 11:
                if i == 1 or i == 14 or i == 7 or i == 8:
                    Pokeball[i][j] = 1
                if (i>1 and i<7) or (i>8 and i<14):
                    Pokeball[i][j] = 3
            if j == 12:
                if i == 2 or i == 13:
                    Pokeball[i][j] = 1
                if i>2 and i<13:
                    Pokeball[i][j] = 3
            if j == 13:
                if i == 2 or i == 13:
                    Pokeball[i][j] = 1
                if i>2 and i<13:
                    Pokeball[i][j] = 3
            if j == 14:
                if i == 3 or i == 12:
                    Pokeball[i][j] = 1
                if i>3 and i<12:
                    Pokeball[i][j] = 3
            if j == 15:
                if i == 4 or i == 5 or i == 10 or i == 11:
                    Pokeball[i][j] = 1
                if i>5 and i<10:
                    Pokeball[i][j] = 3
            if j == 16:
                if i > 5 and i < 10:
                    Pokeball[i][j] = 1
                             
    return Pokeball

#Guinda
def guinda():
    Guinda = []
    for i in range(20):
        Guinda.append([])
        for j in range(20):
            Guinda[i].append(0)

    for i in range(20):
        for j in range(20):
            if j == 11 or j == 15:
                if (i>= 5 and i < 8) or (i>=12 and i<15):
                    Guinda[i][j] = 1
            if i == 4 or i == 8 or i == 11 or i == 15:
                if (j>11 and j<15):
                    Guinda[i][j] = 1
            if (j>11 and j<15):
                if (i>=5 and i<=7) or (i>=12 and i<=14):
                    Guinda[i][j] = 2
    
    Guinda[6][10] = 1
    Guinda[6][9] = 1
    Guinda[7][9] = 1
    Guinda[7][8] = 1
    Guinda[8][8] = 1
    Guinda[8][7] = 1
    Guinda[9][7] = 1
    Guinda[9][6] = 1
    Guinda[10][6] = 1
    Guinda[10][5] = 1
    Guinda[11][5] = 1
    Guinda[12][5] = 1
    Guinda[12][4] = 1
    Guinda[13][4] = 1

    Guinda[7][12] = 3
    Guinda[14][12] = 3


    Guinda[13][10] = 1
    Guinda[13][9] = 1
    Guinda[13][8] = 1
    Guinda[14][8] = 1
    Guinda[14][7] = 1
    Guinda[14][6] = 1
    Guinda[14][5] = 1
    Guinda[14][4] = 1
    Guinda[14][3] = 1
    Guinda[15][3] = 1
    
    return Guinda

#Corazon
def corazon():
    Corazon = []
    for i in range(20):
        Corazon.append([])
        for j in range(20):
            Corazon[i].append(0)

    for i in range(20):
        for j in range(20):
            if j == 3:
                if (i>4 and i<8) or (i>10 and i<14):
                    Corazon[i][j] = 1
            if j == 4:
                if i == 4 or i == 8 or i == 10 or i == 14:
                    Corazon[i][j] = 1
                if (i>4 and i<8) or (i>10 and i<14):
                    Corazon[i][j] = 2
            if j == 5:
                if i == 3 or i == 9 or i == 15:
                    Corazon[i][j] = 1
                if (i>3 and i<9) or (i>9 and i<15):
                    Corazon[i][j] = 2
            if j == 6:
                if i == 2 or i == 16:
                    Corazon[i][j] = 1
                if (i>2 and i<16):
                    Corazon[i][j] = 2
            if j == 7:
                if i == 2 or i == 16:
                    Corazon[i][j] = 1
                if (i>2 and i<16):
                    Corazon[i][j] = 2
            if j == 8:
                if i == 3 or i == 15:
                    Corazon[i][j] = 1
                if (i>3 and i<15):
                    Corazon[i][j] = 2
            if j == 9:
                if i == 3 or i == 15:
                    Corazon[i][j] = 1
                if (i>3 and i<15):
                    Corazon[i][j] = 2
            if j == 10:
                if i == 4 or i == 14:
                    Corazon[i][j] = 1
                if (i>4 and i<14):
                    Corazon[i][j] = 2
            if j == 11:
                if i == 5 or i == 13:
                    Corazon[i][j] = 1
                if (i>5 and i<13):
                    Corazon[i][j] = 2
            if j == 12:
                if i == 6 or i == 12:
                    Corazon[i][j] = 1
                if (i>6 and i<12):
                    Corazon[i][j] = 2
            if j == 13:
                if i == 7 or i == 11:
                    Corazon[i][j] = 1
                if (i>7 and i<11):
                    Corazon[i][j] = 2
            if j == 14:
                if i == 8 or i == 10:
                    Corazon[i][j] = 1
                if (i>8 and i<10):
                    Corazon[i][j] = 2
            if j == 15 and i == 9:
                    Corazon[i][j] = 1
                
    Corazon[5][6] = 3           
    Corazon[6][5] = 3           
             
    return Corazon

#Fuego
def fire():
    Fire = []
    for i in range(10):
        Fire.append([])
        for j in range(10):
            Fire[i].append(0)

    for i in range(10):
        for j in range(10):
            if j == 0:
                if i==4:
                    Fire[i][j] = 11
            if j == 1:
                if i==3 or i == 5 or i == 8:
                    Fire[i][j] = 11
            if j == 2:
                if i == 5 or i == 6:
                    Fire[i][j] = 11
            if j == 3:
                if i == 4 or i == 7:
                    Fire[i][j] = 11
                if i == 5 or i == 6:
                    Fire[i][j] = 12
            if j == 4:
                if i == 3 or i == 7:
                    Fire[i][j] = 11
                if i == 4 or i == 6:
                    Fire[i][j] = 12
                if i == 5:
                    Fire[i][j] = 13
            if j == 5:
                if i == 2 or i == 7:
                    Fire[i][j] = 11
                if i == 3 or i == 4 or i == 6:
                    Fire[i][j] = 12
                if i == 5:
                    Fire[i][j] = 13
            if j == 6:
                if i == 2 or i == 7:
                    Fire[i][j] = 11
                if i == 3 or i == 6:
                    Fire[i][j] = 12
                if i == 4 or i == 5:
                    Fire[i][j] = 13
            if j == 7:
                if i == 2 or i == 6:
                    Fire[i][j] = 11
                if i == 5:
                    Fire[i][j] = 12
                if i == 3 or i == 4:
                    Fire[i][j] = 13
            if j == 8:
                if i == 3 or i == 5:
                    Fire[i][j] = 11
                if i == 4:
                    Fire[i][j] = 13
        
                             
    return Fire

#Gato
def gatitu():
    Gatitu = []
    for i in range(20):
        Gatitu.append([])
        for j in range(32):
            Gatitu[i].append(0)

    for i in range(20):
        for j in range(32):
            if j == 0:
                if i == 6 or i == 14:
                    Gatitu[i][j] = 5
            elif j == 1:
                if i == 5 or i == 7 or i == 13 or i == 15:
                    Gatitu[i][j] = 5
                elif i == 6 or i == 14:
                    Gatitu[i][j] = 7
                
            elif j == 2:
                if i == 4 or i == 8 or i == 12 or i == 16:
                    Gatitu[i][j] = 5
                elif i == 5 or i == 7 or i == 13 or i == 15:
                    Gatitu[i][j] = 6
                elif i == 6 or i == 14:
                    Gatitu[i][j] = 7
                
            elif j == 3:
                if i == 4 or i == 8 or i == 12 or i == 16:
                    Gatitu[i][j] = 5
                elif i == 5 or i == 15:
                    Gatitu[i][j] = 6
                elif i == 6 or i == 14:
                    Gatitu[i][j] = 8
                elif i == 7 or i == 13:
                    Gatitu[i][j] = 7
                
            elif j == 4:
                if i == 3 or (i >= 9 and i <= 11) or i == 17:
                    Gatitu[i][j] = 5
                elif i == 4 or i == 16:
                    Gatitu[i][j] = 6
                elif i == 6 or i == 14 or i == 5 or i == 15:
                    Gatitu[i][j] = 8
                elif i == 7 or i == 8 or i == 13 or i == 12:
                    Gatitu[i][j] = 7
                
            elif j == 5:
                if i == 3 or i == 17:
                    Gatitu[i][j] = 5
                elif i == 4 or i == 16:
                    Gatitu[i][j] = 6
                elif i == 5 or i == 15:
                    Gatitu[i][j] = 8
                elif i >= 6 and i <= 14:
                    Gatitu[i][j] = 7
               
            elif j == 6:
                if i == 2 or i == 18:
                    Gatitu[i][j] = 5
                elif i == 3 or i == 4 or i == 16 or i == 17:
                    Gatitu[i][j] = 6
                elif i >= 5 and i <= 15:
                    Gatitu[i][j] = 7
                
            elif j == 7:
                if i == 2 or i == 18:
                    Gatitu[i][j] = 5
                elif i == 3 or i == 17:
                    Gatitu[i][j] = 6
                elif i == 4 or i == 5 or i == 15 or i == 16 or (i>=8 and i<=12):
                    Gatitu[i][j] = 7
                elif i == 6 or i == 7 or i == 13 or i == 14:
                    Gatitu[i][j] = 1
                
            elif j == 8:
                if i == 2 or i == 18:
                    Gatitu[i][j] = 5
                elif i == 3 or i == 17:
                    Gatitu[i][j] = 6
                elif i == 4 or i == 5 or i == 15 or i == 16 or (i>=8 and i<=12):
                    Gatitu[i][j] = 7
                elif i == 6 or i == 14:
                    Gatitu[i][j] = 1
                elif i == 7 or i == 13:
                    Gatitu[i][j] == 3
                
            elif j == 9:
                if i == 2 or i == 18:
                    Gatitu[i][j] = 5
                elif i == 3 or i == 17:
                    Gatitu[i][j] = 6
                elif i == 4 or i == 5 or i == 15 or i == 16 or (i>=8 and i<=12):
                    Gatitu[i][j] = 7
                elif i == 6 or i == 7 or i == 13 or i == 14:
                    Gatitu[i][j] = 9
                
            elif j == 10:
                if i == 2 or i == 18:
                    Gatitu[i][j] = 5
                elif i == 3 or i == 17:
                    Gatitu[i][j] = 6
                elif i == 4 or i == 5 or i == 15 or i == 16 or (i>=8 and i<=12):
                    Gatitu[i][j] = 7
                elif i == 6 or i == 7 or i == 13 or i == 14:
                    Gatitu[i][j] = 10
                
            elif j == 11:
                if i == 2 or i == 18:
                    Gatitu[i][j] = 5
                elif i == 3 or i == 4 or i == 16 or i == 17:
                    Gatitu[i][j] = 6
                elif i == 5 or i == 6 or i == 7 or i == 13 or i == 14 or i == 15:
                    Gatitu[i][j] = 7
                elif i == 8 or i == 9 or i == 11 or i == 12:
                    Gatitu[i][j] = 3
                elif i == 10:
                    Gatitu[i][j] = 1
                
            elif j == 12:
                if i == 3 or i == 17:
                    Gatitu[i][j] = 5
                elif i == 4 or i == 5 or i == 15 or i == 16 or i == 9 or i == 11:
                    Gatitu[i][j] = 6
                elif i == 6 or i == 14 or i == 10:
                    Gatitu[i][j] = 7
                elif i == 7 or i == 8 or i == 12 or i == 13:
                    Gatitu[i][j] = 3
                
            elif j == 13:
                if i == 4 or i == 5 or i == 15 or i == 16:
                    Gatitu[i][j] = 5
                elif i == 6 or i == 14:
                    Gatitu[i][j] = 6
                elif i >= 7 and i <= 13:
                    Gatitu[i][j] = 7
                
            elif j == 14:
                if i > 4 and i < 16:
                    Gatitu[i][j] = 5

    return Gatitu