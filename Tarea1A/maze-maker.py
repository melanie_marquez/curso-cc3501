import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import sys
import random
import Sprites
import os.path

INT_BYTES = 4

class Controller:
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.newx = 0.0
        self.newy = 0.0
        self.xder = 0.0
        self.yder = 0.0
        self.newxder = 0.0
        self.newyder = 0.0
        self.xscroll = 0.0
        self.yscroll = 0.0
        self.newxscroll = 0.0
        self.newyscroll = 0.0
        self.fillPolygon = True
        self.Inicio = False
        
    def reset(self):
        self.x = 0.0
        self.y = 0.0
        self.newx = 0.0
        self.newy = 0.0
        self.xder = 0.0
        self.yder = 0.0
        self.newxder = 0.0
        self.newyder = 0.0
        self.xscroll = 0.0
        self.yscroll = 0.0
        self.newxscroll = 0.0
        self.newyscroll = 0.0
        self.fillPolygon = True
        self.Inicio = False

controller = Controller()

def mouse_button_callback(window, button, action, mods):
    global controller
    if (button == glfw.MOUSE_BUTTON_1):
        pos = glfw.get_cursor_pos(window)
        controller.x = pos[0]
        controller.y = pos[1]

    if (button == glfw.MOUSE_BUTTON_2):
        pos2 = glfw.get_cursor_pos(window)
        controller.xder = pos2[0]
        controller.yder = pos2[1]

    if (button == glfw.MOUSE_BUTTON_3):
        pos3 = glfw.get_cursor_pos(window)
        controller.xscroll = pos3[0]
        controller.yscroll = pos3[1]
             
def cursor_pos_callback(window, x, y):
    global controller
    controller.mousePos = (x,y)
   
class GPUShape:
    def __init__(self):
        self.vao = 0
        self.vbo = 0
        self.ebo = 0
        self.texture = 0
        self.size = 0

def drawShape(shaderProgram, shape, transform):

    # Binding the proper buffers
    glBindVertexArray(shape.vao)
    glBindBuffer(GL_ARRAY_BUFFER, shape.vbo)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ebo)

    # updating the new transform attribute
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_TRUE, transform)

    # Describing how the data is stored in the VBO
    position = glGetAttribLocation(shaderProgram, "position")
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(0))
    glEnableVertexAttribArray(position)
    
    color = glGetAttribLocation(shaderProgram, "color")
    glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(12))
    glEnableVertexAttribArray(color)

    # This line tells the active shader program to render the active element buffer with the given size
    glDrawElements(GL_TRIANGLES, shape.size, GL_UNSIGNED_INT, None)

def createQuad(color):
    gpuShape = GPUShape()

    vertexData = np.array([
        -0.5, -0.5, 0.0, color[0], color[1], color[2],
        0.5, -0.5, 0.0, color[0], color[1], color[2],
        0.5, 0.5, 0.0, color[3], color[4], color[5],
        -0.5, 0.5, 0.0, color[3], color[4], color[5]
    ], dtype=np.float32)

    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def main():
    # DATOS DE INVOCACION
    x = len(sys.argv)
    editar = False
    matrizEditar = 0
    if x == 3:
        nombreLab = sys.argv[1]
        tamaño = sys.argv[2].split("x")
        tamañoLabX = int(tamaño[0])
        tamañoLabY = int(tamaño[1])
        if os.path.isfile(nombreLab):
            print("Error: Archivo ya existente")
            return
    else:
        nombreLab = sys.argv[1]
        matrizEditar = np.load(nombreLab)
        tamañoLabX = len(matrizEditar)
        tamañoLabY = len(matrizEditar[0])
        editar = True

    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, nombreLab, None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    glfw.set_cursor_pos_callback(window, cursor_pos_callback)
    glfw.set_mouse_button_callback(window, mouse_button_callback)

    vertex_shader = """
    #version 130
    in vec3 position;
    in vec3 color;

    out vec3 fragColor;

    uniform mat4 transform;

    void main()
    {
        fragColor = color;
        gl_Position = transform * vec4(position, 1.0f);
    }
    """

    fragment_shader = """
    #version 130

    in vec3 fragColor;
    out vec4 outColor;

    void main()
    {
        outColor = vec4(fragColor, 1.0f);
    }
    """

    shaderProgram = OpenGL.GL.shaders.compileProgram(
        OpenGL.GL.shaders.compileShader(vertex_shader, GL_VERTEX_SHADER),
        OpenGL.GL.shaders.compileShader(fragment_shader, GL_FRAGMENT_SHADER))

    glUseProgram(shaderProgram)

    glClearColor(0.85, 0.85, 0.85, 1.0)
    
    #Colores
    Pink = [255/255,31/255,134/255,255/255,31/255,134/255]
    Black = [0.0,0.0,0.0,0.0,0.0,0.0]
    White = [1.0,1.0,1.0,1.0,1.0,1.0]
    Red = [1.0,0.0,0.0,1.0,0.0,0.0]
    Cafe = [87/255,27/255,0/255,87/255,27/255,0/255]
    CafeClarito = [255/255,209/255,140/255,255/255,209/255,140/255]
    Gris = [240/255,233/255,223/255,240/255,233/255,223/255]
    Rosita = [255/255,106/255,223/255,255/255,106/255,223/255]
    AmarilloOscuro = [214/255,164/255,39/255,214/255,164/255,39/255]
    AmarilloClarito = [232/255,206/255,39/255,232/255,206/255,39/255]
    Fondo = [1.0,1.0,1.0,255/255,31/255,134/255]

    # Si se dio tamaño, crear un nuevo laberinto, si no, entonces editar el archivo dado
    if not editar:
        matrizFill = np.empty((tamañoLabX,tamañoLabY))
        for i in range(tamañoLabX):
           for j in range(tamañoLabY):
                if i == 0 or i == tamañoLabX-1 or j == 0 or j == tamañoLabY-1:
                    matrizFill[i][j] = 1
                else:
                    matrizFill[i][j] = 0
    
    else:
        matrizFill = matrizEditar

    #Cuadrados de distintos colores
    gpuQuadBlack = createQuad(Black)
    gpuQuadPink = createQuad(Pink)
    gpuQuadWhite = createQuad(White)
    gpuQuadFondo = createQuad(Fondo)

    # Crear cuadrados para tesoros
    # GUINDA:
    tesoroGuinda = Sprites.guinda()

    gpuGuinda = []
    for i in range(21):
        gpuGuinda.append([])
    for j in range(21):
        gpuGuinda[i].append(None)

    for i in range(20):
        for j in range(20):
            if tesoroGuinda[i][j] == 1:
                gpuGuinda[i].append(createQuad(Black))  
            elif tesoroGuinda[i][j] == 2:
                gpuGuinda[i].append(createQuad(Red))
            elif tesoroGuinda[i][j] == 3:
                gpuGuinda[i].append(createQuad(White))
            else:
                gpuGuinda[i].append(createQuad(White))  
        
    # CORAZON:
    tesoroCorazon = Sprites.corazon()

    gpuCorazon = []
    for i in range(21):
        gpuCorazon.append([])
    for j in range(21):
        gpuCorazon[i].append(None)

    for i in range(20):
        for j in range(20):
            if tesoroCorazon[i][j] == 1:
                gpuCorazon[i].append(createQuad(Black))  
            elif tesoroCorazon[i][j] == 2:
                gpuCorazon[i].append(createQuad(Red))
            elif tesoroCorazon[i][j] == 3:
                gpuCorazon[i].append(createQuad(White))
            else:
                gpuCorazon[i].append(createQuad(White))  

    # Pokeball:
    tesoroPok = Sprites.pokeball()

    gpuPok = []
    for i in range(21):
        gpuPok.append([])
    for j in range(21):
        gpuPok[i].append(None)

    for i in range(20):
        for j in range(20):
            if tesoroPok[i][j] == 1:
                gpuPok[i].append(createQuad(Black))  
            elif tesoroPok[i][j] == 2:
                gpuPok[i].append(createQuad(Red))
            elif tesoroPok[i][j] == 3:
                gpuPok[i].append(createQuad(White))
            else:
                gpuPok[i].append(createQuad(White))  

    # Gatitu:
    gatitu = Sprites.gatitu()

    gpuGatitu = []
    for i in range(21):
        gpuGatitu.append([])
    for j in range(33):
        gpuGatitu[i].append(None)

    for i in range(20):
        for j in range(32):
            if gatitu[i][j] == 1:
                gpuGatitu[i].append(createQuad(Black))  
            elif gatitu[i][j] == 3:
                gpuGatitu[i].append(createQuad(White))
            elif gatitu[i][j] == 5:
                gpuGatitu[i].append(createQuad(Cafe))
            elif gatitu[i][j] == 6:
                gpuGatitu[i].append(createQuad(CafeClarito))
            elif gatitu[i][j] == 7:
                gpuGatitu[i].append(createQuad(Gris)) 
            elif gatitu[i][j] == 8:
                gpuGatitu[i].append(createQuad(Rosita))
            elif gatitu[i][j] == 9:
                gpuGatitu[i].append(createQuad(AmarilloOscuro))
            elif gatitu[i][j] == 10:
                gpuGatitu[i].append(createQuad(AmarilloClarito))  
            else:
                gpuGatitu[i].append(createQuad(White)) 
    
    while not glfw.window_should_close(window):
        glfw.poll_events()

        glClear(GL_COLOR_BUFFER_BIT)

        cuadradoXscroll = -1
        cuadradoYscroll = -1
        posxscroll = controller.xscroll
        posyscroll = controller.yscroll
        newposxscroll = controller.newxscroll
        newposyscroll = controller.newyscroll

        if posxscroll != newposxscroll and posyscroll != newposyscroll:
            aux = max(tamañoLabX,tamañoLabY)
            aux = 600 / aux
            cuadradoXscroll = posxscroll // aux
            cuadradoYscroll = posyscroll // aux

        cuadradoX = -1
        cuadradoY = -1
        posx = controller.x
        posy = controller.y
        newposx = controller.newx
        newposy = controller.newy

        if posx != newposx and posy != newposy:
            aux = max(tamañoLabX,tamañoLabY)
            aux = 600 / aux
            cuadradoX = posx // aux
            cuadradoY = posy // aux

        cuadradoXder = -1
        cuadradoYder = -1
        posxder = controller.xder
        posyder = controller.yder
        newposxder = controller.newxder
        newposyder = controller.newyder

        if posxder != newposxder and posyder != newposyder:
            aux = max(tamañoLabX,tamañoLabY)
            aux = 600 / aux
            cuadradoXder = posxder // aux
            cuadradoYder = posyder // aux

        #Fondo
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
        drawShape(shaderProgram, gpuQuadFondo, tr.uniformScale(4))

        # Dibujar todo 
        escala = (20/max(tamañoLabX,tamañoLabY))/10
        tras = (max(tamañoLabX,tamañoLabY)-1)/2
        cambio_de_tamaño = tr.uniformScale(escala)
        t = cambio_de_tamaño.dot(tr.translate(tras*-1,tras,0.0))
        for i in range(tamañoLabX):
            for j in range(tamañoLabY):
                transform1 = t.dot(tr.translate(i,j*-1,0.0))
                if (i == cuadradoX and j == cuadradoY) and matrizFill[i][j] == 1:
                    matrizFill[i][j] = 0
                elif (i == cuadradoX and j == cuadradoY) and matrizFill[i][j] == 0:
                    matrizFill[i][j] = 1
                
                if (i == cuadradoXder and j == cuadradoYder) and matrizFill[i][j] == 0:
                    matrizFill[i][j] = 3
                elif (i == cuadradoXder and j == cuadradoYder) and matrizFill[i][j] == 3:
                    matrizFill[i][j] = 4
                elif (i == cuadradoXder and j == cuadradoYder) and matrizFill[i][j] == 4:
                    matrizFill[i][j] = 5
                elif (i == cuadradoXder and j == cuadradoYder) and matrizFill[i][j] == 5:
                    matrizFill[i][j] = 0

                if (i == cuadradoXscroll and j == cuadradoYscroll) and matrizFill[i][j] == 0 and controller.Inicio == False:
                    controller.Inicio = True
                    matrizFill[i][j] = 2
                elif (i == cuadradoXscroll and j == cuadradoYscroll) and matrizFill[i][j] == 2:
                    controller.Inicio = False
                    matrizFill[i][j] = 0
                

                if matrizFill[i][j] == 0:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)
                elif matrizFill[i][j] == 1:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadPink, transform1)
                    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE)
                    drawShape(shaderProgram, gpuQuadBlack, transform1)
                elif matrizFill[i][j] == 2:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)
                    #Dibujar gatitu
                    esc = 1/(max(tamañoLabX,tamañoLabY)*10)
                    cambio_de_tamaño5 = tr.uniformScale(esc)
                    t5 = cambio_de_tamaño5.dot(tr.translate(-10*max(tamañoLabX,tamañoLabY),10*max(tamañoLabX,tamañoLabY),0.0))
                    t5 = t5.dot(tr.translate((20*i),(20*-j),0.0))
                    for k in range(20):
                        for l in range(32):
                            transform5 = t5.dot(tr.translate(k,l*-1,0.0))
                            if gatitu[k][l] != 0:
                                drawShape(shaderProgram, gpuGatitu[k][l], transform5) 
                elif matrizFill[i][j] == 3:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)  
                    #Dibujar guinda
                    esc = 1/(max(tamañoLabX,tamañoLabY)*10)
                    cambio_de_tamaño2 = tr.uniformScale(esc)
                    t2 = cambio_de_tamaño2.dot(tr.translate(-10*max(tamañoLabX,tamañoLabY),10*max(tamañoLabX,tamañoLabY),0.0))
                    t2 = t2.dot(tr.translate(20*i,20*-j,0.0))
                    for k in range(20):
                        for l in range(20):
                            transform2 = t2.dot(tr.translate(k,l*-1,0.0))
                            if tesoroGuinda[k][l] != 0:
                                drawShape(shaderProgram, gpuGuinda[k][l], transform2)
                elif matrizFill[i][j] == 4:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)  
                    #Dibujar corazon
                    esc = 1/(max(tamañoLabX,tamañoLabY)*10)
                    cambio_de_tamaño3 = tr.uniformScale(esc)
                    t3 = cambio_de_tamaño3.dot(tr.translate(-10*max(tamañoLabX,tamañoLabY),10*max(tamañoLabX,tamañoLabY),0.0))
                    t3 = t3.dot(tr.translate(20*i,20*-j,0.0))
                    for k in range(20):
                        for l in range(20):
                            transform3 = t3.dot(tr.translate(k,l*-1,0.0))
                            if tesoroCorazon[k][l] != 0:
                                drawShape(shaderProgram, gpuCorazon[k][l], transform3)  
                elif matrizFill[i][j] == 5:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)  
                    #Dibujar pokeball
                    esc = 1/(max(tamañoLabX,tamañoLabY)*10)
                    cambio_de_tamaño4 = tr.uniformScale(esc)
                    t4 = cambio_de_tamaño4.dot(tr.translate(-10*max(tamañoLabX,tamañoLabY),10*max(tamañoLabX,tamañoLabY),0.0))
                    t4 = t4.dot(tr.translate(20*i,20*-j,0.0))
                    for k in range(20):
                        for l in range(20):
                            transform4 = t4.dot(tr.translate(k,l*-1,0.0))
                            if tesoroPok[k][l] != 0:
                               drawShape(shaderProgram, gpuPok[k][l], transform4)  
 

        controller.newx = posx
        controller.newy = posy

        controller.newxder = posxder
        controller.newyder = posyder

        controller.newxscroll = posxscroll
        controller.newyscroll = posyscroll

        glfw.swap_buffers(window)
    np.save(nombreLab,matrizFill)
    glfw.terminate()

controller.reset()
main()