import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import sys
import Sprites
import os.path
import time
import random

INT_BYTES = 4

class Controller:
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.fillPolygon = True
        self.dark = False
        self.pasos = 0
        self.kill = False
        
    def reset(self):
        self.x = 0.0
        self.y = 0.0
        self.fillPolygon = True
        self.dark = False
        self.pasos = 0
        self.kill = False

controller = Controller()

def on_key(window, key, scancode, action, mods):
    global controller
    if action != glfw.PRESS:
        return
    elif key == glfw.KEY_SPACE:
        controller.dark = not controller.dark
    elif key == glfw.KEY_ESCAPE:
        sys.exit()
    elif key == glfw.KEY_LEFT:
        controller.x = -1
    elif key == glfw.KEY_RIGHT:
        controller.x = 1
    elif key == glfw.KEY_UP:
        controller.y = 1
    elif key == glfw.KEY_DOWN:
        controller.y = -1
    elif key == glfw.KEY_1:
        controller.kill = True
    else:
        print('Unknown key')
   
class GPUShape:
    def __init__(self):
        self.vao = 0
        self.vbo = 0
        self.ebo = 0
        self.texture = 0
        self.size = 0

def drawShape(shaderProgram, shape, transform):

    # Binding the proper buffers
    glBindVertexArray(shape.vao)
    glBindBuffer(GL_ARRAY_BUFFER, shape.vbo)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ebo)

    # updating the new transform attribute
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_TRUE, transform)

    # Describing how the data is stored in the VBO
    position = glGetAttribLocation(shaderProgram, "position")
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(0))
    glEnableVertexAttribArray(position)
    
    color = glGetAttribLocation(shaderProgram, "color")
    glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(12))
    glEnableVertexAttribArray(color)

    # This line tells the active shader program to render the active element buffer with the given size
    glDrawElements(GL_TRIANGLES, shape.size, GL_UNSIGNED_INT, None)

def createQuad(color):
    gpuShape = GPUShape()

    vertexData = np.array([
        -0.5, -0.5, 0.0, color[0], color[1], color[2],
        0.5, -0.5, 0.0, color[0], color[1], color[2],
        0.5, 0.5, 0.0, color[3], color[4], color[5],
        -0.5, 0.5, 0.0, color[3], color[4], color[5]
    ], dtype=np.float32)

    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def main():

    #DATOS DE INVOCACION DEL PROGRAMA
    nombreLab = sys.argv[1]
    if not os.path.isfile(nombreLab):
            print("Error: Archivo no existente")
            return
    matrizFill = np.load(nombreLab)
    tamLabX = len(matrizFill)
    tamLabY = len(matrizFill[0])

    #ERROR SI NO HAY TESOROS O SI NO ESTA DEFINIDA LA POSICION DE INICIO
    if not(3 in matrizFill) and  not(4 in matrizFill) and not(5 in matrizFill):
        print("Error! No hay tesoros en el laberinto seleccionado :C")
        return
    if not (2 in matrizFill):
        print("Error! No se ha definido la posicion inicial del gatitu")
        return 


    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, nombreLab, None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    glfw.set_key_callback(window, on_key)

    vertex_shader = """
    #version 130
    in vec3 position;
    in vec3 color;

    out vec3 fragColor;

    uniform mat4 transform;

    void main()
    {
        fragColor = color;
        gl_Position = transform * vec4(position, 1.0f);
    }
    """

    fragment_shader = """
    #version 130

    in vec3 fragColor;
    out vec4 outColor;

    void main()
    {
        outColor = vec4(fragColor, 1.0f);
    }
    """

    shaderProgram = OpenGL.GL.shaders.compileProgram(
        OpenGL.GL.shaders.compileShader(vertex_shader, GL_VERTEX_SHADER),
        OpenGL.GL.shaders.compileShader(fragment_shader, GL_FRAGMENT_SHADER))

    glUseProgram(shaderProgram)

    glClearColor(0.85, 0.85, 0.85, 1.0)
    
    #COLORES 
    Pink = [255/255,31/255,134/255,255/255,31/255,134/255]
    Black = [0.0,0.0,0.0,0.0,0.0,0.0]
    White = [1.0,1.0,1.0,1.0,1.0,1.0]
    Red = [1.0,0.0,0.0,1.0,0.0,0.0]
    Cafe = [87/255,27/255,0/255,87/255,27/255,0/255]
    CafeClarito = [255/255,209/255,140/255,255/255,209/255,140/255]
    Gris = [240/255,233/255,223/255,240/255,233/255,223/255]
    Rosita = [255/255,106/255,223/255,255/255,106/255,223/255]
    AmarilloOscuro = [214/255,164/255,39/255,214/255,164/255,39/255]
    AmarilloClarito = [232/255,206/255,39/255,232/255,206/255,39/255]
    Fondo = [1.0,1.0,1.0,255/255,31/255,134/255]
    FondoDark = [1.0,0.0,0.0,0.0,0.0,0.0]
    Naranja1 = [255/255,14/255,30/255,255/255,14/255,30/255]
    Naranja2 = [254/255,112/255,2/255,254/255,112/255,2/255]
    Amarillo = [255/255,222/255,3/255,255/255,222/255,3/255]

    #CUADRADOS DE DINTINTOS COLORES
    gpuQuadBlack = createQuad(Black)
    gpuQuadRed = createQuad(Red)
    gpuQuadPink = createQuad(Pink)
    gpuQuadWhite = createQuad(White)
    gpuQuadFondo = createQuad(Fondo)
    gpuQuadFondoDark = createQuad(FondoDark)

    # CREANDO ESPACIO PARA TESOROS

    # GUINDA:
    tesoroGuinda = Sprites.guinda()
    gpuGuinda = []
    for i in range(21):
        gpuGuinda.append([])
    for j in range(21):
        gpuGuinda[i].append(None)
    for i in range(20):
        for j in range(20):
            if tesoroGuinda[i][j] == 1:
                gpuGuinda[i].append(createQuad(Black))  
            elif tesoroGuinda[i][j] == 2:
                gpuGuinda[i].append(createQuad(Red))
            elif tesoroGuinda[i][j] == 3:
                gpuGuinda[i].append(createQuad(White))
            else:
                gpuGuinda[i].append(createQuad(White))  

    # Fire:
    fire = Sprites.fire()
    gpuFire = []
    for i in range(11):
        gpuFire.append([])
    for j in range(21):
        gpuFire[i].append(None)
    for i in range(10):
        for j in range(10):
            if fire[i][j] == 11:
                gpuFire[i].append(createQuad(Naranja1))  
            elif fire[i][j] == 12:
                gpuFire[i].append(createQuad(Naranja2))
            elif fire[i][j] == 13:
                gpuFire[i].append(createQuad(Amarillo))
            else:
                gpuFire[i].append(createQuad(White)) 

    for i in range(max(tamLabX,tamLabY)):
        random1 = random.randint(0,tamLabX-1)
        random2 = random.randint(0,tamLabY-1)
        if matrizFill[random1][random2] == 0:
            matrizFill[random1][random2] = 6
        
    # CORAZON:
    tesoroCorazon = Sprites.corazon()
    gpuCorazon = []
    for i in range(21):
        gpuCorazon.append([])
    for j in range(21):
        gpuCorazon[i].append(None)
    for i in range(20):
        for j in range(20):
            if tesoroCorazon[i][j] == 1:
                gpuCorazon[i].append(createQuad(Black))  
            elif tesoroCorazon[i][j] == 2:
                gpuCorazon[i].append(createQuad(Red))
            elif tesoroCorazon[i][j] == 3:
                gpuCorazon[i].append(createQuad(White))
            else:
                gpuCorazon[i].append(createQuad(White))  

    # Pokeball:
    tesoroPok = Sprites.pokeball()
    gpuPok = []
    for i in range(21):
        gpuPok.append([])
    for j in range(21):
        gpuPok[i].append(None)
    for i in range(20):
        for j in range(20):
            if tesoroPok[i][j] == 1:
                gpuPok[i].append(createQuad(Black))  
            elif tesoroPok[i][j] == 2:
                gpuPok[i].append(createQuad(Red))
            elif tesoroPok[i][j] == 3:
                gpuPok[i].append(createQuad(White))
            else:
                gpuPok[i].append(createQuad(White))  

    # Gatitu:
    gatitu = Sprites.gatitu()
    gpuGatitu = []
    for i in range(21):
        gpuGatitu.append([])
    for j in range(33):
        gpuGatitu[i].append(None)
    for i in range(20):
        for j in range(32):
            if gatitu[i][j] == 1:
                gpuGatitu[i].append(createQuad(Black))  
            elif gatitu[i][j] == 3:
                gpuGatitu[i].append(createQuad(White))
            elif gatitu[i][j] == 5:
                gpuGatitu[i].append(createQuad(Cafe))
            elif gatitu[i][j] == 6:
                gpuGatitu[i].append(createQuad(CafeClarito))
            elif gatitu[i][j] == 7:
                gpuGatitu[i].append(createQuad(Gris)) 
            elif gatitu[i][j] == 8:
                gpuGatitu[i].append(createQuad(Rosita))
            elif gatitu[i][j] == 9:
                gpuGatitu[i].append(createQuad(AmarilloOscuro))
            elif gatitu[i][j] == 10:
                gpuGatitu[i].append(createQuad(AmarilloClarito))  
            else:
                gpuGatitu[i].append(createQuad(White)) 


    tiempoInicial = glfw.get_time()
    while not glfw.window_should_close(window):
        #Cambiar posicion de los malulos
        aux = glfw.get_time()//1
        if(aux%3 == 0):
            for i in range(tamLabX):
                for j in range(tamLabY):
                    if matrizFill[i][j] == 6:
                        r = random.randint(0,3)
                        if (i+1)!=tamLabX:
                            if r == 0 and (matrizFill[i+1][j] == 0 or matrizFill[i+1][j] == 2):
                                if matrizFill[i+1][j] == 2:
                                    matrizFill[i+1][j] = 6
                                    matrizFill[i][j] = 0
                                    print("you die!")
                                    return
                                matrizFill[i+1][j] = 6
                                matrizFill[i][j] = 0
                                
                        if (i-1)!=-1:
                            if r == 1 and (matrizFill[i-1][j] == 0 or matrizFill[i-1][j] == 2):
                                if matrizFill[i-1][j] == 2:
                                    matrizFill[i-1][j] = 6
                                    matrizFill[i][j] = 0
                                    print("you die!")
                                    return
                                matrizFill[i-1][j] = 6
                                matrizFill[i][j] = 0
                                
                        if (j+1)!=tamLabY:
                            if r == 2 and (matrizFill[i][j+1] == 0 or matrizFill[i][j+1] == 2):
                                if matrizFill[i][j+1] == 2:
                                    matrizFill[i][j+1] = 6
                                    matrizFill[i][j] = 0
                                    print("you die!")
                                    return 
                                
                        if (j-1)!=-1:
                            if r == 3 and (matrizFill[i][j-1] == 0 or matrizFill[i][j-1] == 2):
                                if matrizFill[i][j-1] == 2:
                                    matrizFill[i][j-1] = 6
                                    matrizFill[i][j] = 0
                                    print("you die!")
                                    return
                                
                                
            
        glfw.poll_events()

        glClear(GL_COLOR_BUFFER_BIT)

        #DIBUJAR FONDO
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
        if not controller.dark:
            drawShape(shaderProgram, gpuQuadFondo, tr.uniformScale(4))
        else:
            drawShape(shaderProgram, gpuQuadFondoDark, tr.uniformScale(4))

        #CONTROLAR MOVIMIENTO DEL GATO
        indiceX = np.where(matrizFill == 2)[0]
        indiceY = np.where(matrizFill == 2)[1]
        if controller.x != 0:
            if int(indiceX)+1 != (tamLabX):
                if controller.x > 0 and matrizFill[int(indiceX)+1][int(indiceY)] != 1:
                    if matrizFill[int(indiceX)+1][int(indiceY)] == 6:
                        print("you die!")
                        return
                    matrizFill[int(indiceX)][int(indiceY)] = 0
                    matrizFill[int(indiceX)+1][int(indiceY)] = 2
                    controller.pasos +=1
            if int(indiceX)-1 != -1:        
                if controller.x < 0 and matrizFill[int(indiceX)-1][int(indiceY)] != 1:
                    if matrizFill[int(indiceX)-1][int(indiceY)] == 6:
                        print("you die!")
                        return
                    matrizFill[int(indiceX)][int(indiceY)] = 0
                    matrizFill[int(indiceX)-1][int(indiceY)] = 2
                    controller.pasos +=1

        controller.x = 0
                        
        if controller.y != 0:
            if int(indiceY)+1 != (tamLabY):
                if controller.y < 0 and matrizFill[int(indiceX)][int(indiceY)+1] != 1:
                    if matrizFill[int(indiceX)][int(indiceY)+1] == 6:
                        print("you die!")
                        return
                    matrizFill[int(indiceX)][int(indiceY)] = 0
                    matrizFill[int(indiceX)][int(indiceY)+1] = 2
                    controller.pasos +=1
            if int(indiceY)-1 != -1:    
                if controller.y > 0 and matrizFill[int(indiceX)][int(indiceY)-1] != 1:
                    if matrizFill[int(indiceX)][int(indiceY)-1] == 6:
                        print("you die!")
                        return
                    matrizFill[int(indiceX)][int(indiceY)] = 0
                    matrizFill[int(indiceX)][int(indiceY)-1] = 2
                    controller.pasos +=1

        controller.y = 0

        if controller.kill:
            if matrizFill[int(indiceX)+1][int(indiceY)] == 6:
                matrizFill[int(indiceX)+1][int(indiceY)] = 0
            if matrizFill[int(indiceX)-1][int(indiceY)] == 6:
                matrizFill[int(indiceX)-1][int(indiceY)] = 0   
            if matrizFill[int(indiceX)][int(indiceY)+1] == 6:
                matrizFill[int(indiceX)][int(indiceY)+1] = 0
            if matrizFill[int(indiceX)][int(indiceY)-1] == 6:
                matrizFill[int(indiceX)][int(indiceY)-1] = 0
        controller.kill = False

        #DIBUJAR COSAS
        escala = (20/max(tamLabX,tamLabY))/10
        tras = (max(tamLabX,tamLabY)-1)/2
        cambio_de_tam = tr.uniformScale(escala)
        t = cambio_de_tam.dot(tr.translate(tras*-1,tras,0.0))
        for i in range(tamLabX):
            for j in range(tamLabY):
                transform1 = t.dot(tr.translate(i,j*-1,0.0))            
                if matrizFill[i][j] == 0:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)  
                elif matrizFill[i][j] == 1:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadPink, transform1)
                    if not controller.dark:
                        glPolygonMode(GL_FRONT_AND_BACK,GL_LINE)
                    drawShape(shaderProgram, gpuQuadBlack, transform1) 
                    if controller.dark:
                        glPolygonMode(GL_FRONT_AND_BACK,GL_LINE)
                        drawShape(shaderProgram, gpuQuadRed, transform1)
                elif matrizFill[i][j] == 2:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)
                    #Dibujar gatitu
                    esc = 1/(max(tamLabX,tamLabY)*10)
                    cambio_de_tamaño5 = tr.uniformScale(esc)
                    t5 = cambio_de_tamaño5.dot(tr.translate(-10*max(tamLabX,tamLabY),10*max(tamLabX,tamLabY),0.0))
                    t5 = t5.dot(tr.translate((20*i),(20*-j),0.0))
                    for k in range(20):
                        for l in range(32):
                            transform5 = t5.dot(tr.translate(k,l*-1,0.0))
                            if gatitu[k][l] != 0:
                                drawShape(shaderProgram, gpuGatitu[k][l], transform5) 
                elif matrizFill[i][j] == 3 :
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1)  
                    if not controller.dark:
                        #Dibujar guinda
                        esc = 1/(max(tamLabX,tamLabY)*10)
                        cambio_de_tamaño2 = tr.uniformScale(esc)
                        t2 = cambio_de_tamaño2.dot(tr.translate(-10*max(tamLabX,tamLabY),10*max(tamLabX,tamLabY),0.0))
                        t2 = t2.dot(tr.translate(20*i,20*-j,0.0))
                        for k in range(20):
                            for l in range(20):
                                transform2 = t2.dot(tr.translate(k,l*-1,0.0))
                                if tesoroGuinda[k][l] != 0:
                                    drawShape(shaderProgram, gpuGuinda[k][l], transform2)
                elif matrizFill[i][j] == 4:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1) 
                    if not controller.dark: 
                        #Dibujar corazon
                        esc = 1/(max(tamLabX,tamLabY)*10)
                        cambio_de_tamaño3 = tr.uniformScale(esc)
                        t3 = cambio_de_tamaño3.dot(tr.translate(-10*max(tamLabX,tamLabY),10*max(tamLabX,tamLabY),0.0))
                        t3 = t3.dot(tr.translate(20*i,20*-j,0.0))
                        for k in range(20):
                            for l in range(20):
                                transform3 = t3.dot(tr.translate(k,l*-1,0.0))
                                if tesoroCorazon[k][l] != 0:
                                    drawShape(shaderProgram, gpuCorazon[k][l], transform3)  
                elif matrizFill[i][j] == 5:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1) 
                    if not controller.dark: 
                        #Dibujar pokeball
                        esc = 1/(max(tamLabX,tamLabY)*10)
                        cambio_de_tamaño4 = tr.uniformScale(esc)
                        t4 = cambio_de_tamaño4.dot(tr.translate(-10*max(tamLabX,tamLabY),10*max(tamLabX,tamLabY),0.0))
                        t4 = t4.dot(tr.translate(20*i,20*-j,0.0))
                        for k in range(20):
                            for l in range(20):
                                transform4 = t4.dot(tr.translate(k,l*-1,0.0))
                                if tesoroPok[k][l] != 0:
                                    drawShape(shaderProgram, gpuPok[k][l], transform4)  
                elif matrizFill[i][j] == 6:
                    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                    drawShape(shaderProgram, gpuQuadWhite, transform1) 
                    if controller.dark: 
                        #Dibujar malulo
                        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL)
                        esc = 1/(max(tamLabX,tamLabY)*5)
                        cambio_de_tamaño5 = tr.uniformScale(esc)
                        t5 = cambio_de_tamaño5.dot(tr.translate(-5*max(tamLabX,tamLabY),5*max(tamLabX,tamLabY),0.0))
                        t5 = t5.dot(tr.translate(10*i,10*-j,0.0))
                        for k in range(10):
                            for l in range(10):
                                transform5 = t5.dot(tr.translate(k,l*-1,0.0))
                                if fire[k][l] != 0:
                                    drawShape(shaderProgram, gpuFire[k][l], transform5)  
          
        #JUEGO TERMINADO
        if not(3 in matrizFill) and not( 4 in matrizFill) and not(5 in matrizFill):
            print("You win")
            tiempo = (glfw.get_time() - tiempoInicial)//1
            print("Pasaron",tiempo,"segundos!")
            print("Se dieron",controller.pasos,"pasos!")
            return
        
        glfw.swap_buffers(window)
    
    glfw.terminate()

controller.reset()
main()